class kerberos (
    Array[String] $kdcs,
    Array[String] $otp_servers,
    Array[Hash] $domains,
    String $realm,
    String $pam_package,
    String $admin_server,
    String $default_domain,
    Boolean $use_2fa,
    Boolean $use_pam,
) {
    file { 'krb5.conf':
        path => '/etc/krb5.conf',
        ensure => file,
	owner => 'root',
	group => 'root',
        mode => '0644',
        content => epp('kerberos/etc/krb5.conf.epp', {
	    'kdcs' => $kdcs,
	    'domains' => $domains,
	    'realm' => $realm,
	    'admin_server' => $admin_server,
	    'default_domain' => $default_domain,
	}),
    }

    if $use_2fa {
	package { 'pam_otp' : ensure => present,}
	package { 'pam_krb5' : ensure => present,}
	file {'sshd':
	    path => '/etc/pam.d/sshd',
	    ensure => file,
	    owner => 'root',
	    group => 'root',
	    mode => '0644',
	    content => epp('kerberos/etc/pam.d/sshd.epp', {
		'otp_servers' => $otp_servers,
		'realm' => $realm,
	    }),
	}
    }

    if $use_pam {
	package {"$pam_package":
	    ensure => 'present',
	}
    }
}
